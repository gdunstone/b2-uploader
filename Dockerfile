FROM alpine:latest
RUN apk add --no-cache bash inotify-tools jq py3-pip && \
    pip3 install b2 

ENV UPLOAD_PREFIX ""
ENV B2_ACCOUNT_ID ""
ENV B2_APPLICATION_KEY ""
ENV B2_BUCKET ""

COPY inotify-upload /usr/bin/inotify-upload
CMD ["inotify-upload"]