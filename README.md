Container registry path:

`registry.gitlab.com/gdunstone/b2-uploader`

recommended tag:

```
registry.gitlab.com/gdunstone/b2-uploader:v0.0.5
registry.gitlab.com/gdunstone/b2-uploader:latest
```